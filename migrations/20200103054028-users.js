'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  db.createTable('users', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      username: {
        type: 'string',
        length: 180,
        notNull: true
      },
      first_name: {
        type: 'string',
        length: 255,
        notNull: true
      },
      last_name: {
        type: 'string',
        length: 255,
        notNull: true
      },
      email: {
        type: 'string',
        length: 255,
        notNull: true
      },
      enabled: {
        type: 'boolean',
        defaultValue: true,
      },
      password: {
        type: 'string',
        length: 255,
        notNull: true
      },
      created_by: {
        type: 'int',
        notNull: true
      },
      updated_by: {
        type: 'int'
      },
      created_at: {
        type: 'timestamp',
        defaultValue: 'CURRENT_TIMESTAMP',
        notNull: true
      },
      updated_at: {
        type: 'datetime'
      },
      deleted_at: {
        type: 'datetime'
      }
    },
    ifNotExists: true
  }, function (err) {
    if (err) return callback(err);
    else {
      db.insert('users',
        ['username', 'first_name', 'last_name', 'email', 'password', 'created_by'],
        ['admin', 'first', 'last', 'admin@admin.com', '38e24912471365aed567e675b107e875', 0],
        function (err) {
          if (err) return callback(err);
          return callback();
        }
      )
    }
    return callback();
  });
};

exports.down = function (db, callback) {
  db.dropTable('users', callback);
};

exports._meta = {
  "version": 1
};
