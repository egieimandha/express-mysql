const jwt = require('jsonwebtoken');
const { findById } = require('../models/user');
const { unauthorizedResponse } = require('../utility/apiResponse');

const authenticate = (req, res, next) => {
	const authorization = req.headers['authorization'];
	if (authorization) {
		const token = authorization.replace('Bearer ', '').replace('bearer ', '');
		try {
			const decoded = jwt.verify(token, config.jwtSecret);
			console.log('MASUPP', decoded)
			if (decoded) {

				return findById(decoded.sub, (err, response) => {
					if (!err && response) {
						req.user = response;
						return next();
					}
					return unauthorizedResponse(res, 'Authentication failed (tokens).')
				});
			}
		} catch (e) {
			console.log('errors (%s): %s', e.name, e.message);
		}

	}

	return unauthorizedResponse(res, 'Authentication failed (tokens).')
}

module.exports = authenticate;
